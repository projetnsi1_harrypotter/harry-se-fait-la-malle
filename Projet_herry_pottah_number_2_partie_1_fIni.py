# coding: utf8



'''

Mini-projet "Harry se fait la malle"



Liste de fournitures scolaires



Auteur : David Landry

'''



fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

poids_maximal = 4

def remplir_malle(poids_max):
    contenu_malle = []
    poids_malle = 0
    for i in range(len(fournitures_scolaires)):
        if fournitures_scolaires[i]['Poids'] + poids_malle < poids_max:
            contenu_malle.append(fournitures_scolaires[i])
            poids_malle += fournitures_scolaires[i]['Poids']
    return(contenu_malle, poids_malle)

malle_harry = remplir_malle(4)

print("La malle de Harry contient :\n")
for i in range(len(malle_harry) + 2):
    print(f"- {malle_harry[0][i]['Nom'] :<20} \
({malle_harry[0][i]['Poids']} kg)")
print(f"\nLa malle pèse en tout {malle_harry[1]} kg.")
    
