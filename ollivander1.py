# -*- coding: utf-8 -*-

def ollivander():
    """
    Cette fonction permet d'exécuter tout ce qu'il se passe 
    dans la boutique de ollivander. 
    Elle demande d'abord demander a l'utilisateur si il veut rentrer 
    sa propre somme a rendre ou si il veut juste faire les tests 
    intégrés. 
    
    
    """
    
    choix = str(input('Si vous souhaitez essayer ce programme avec les tests \
entrez "test", si vous voulez saisir vous meme le motant \
a rendre, saisissez "manuel" : '))
    print('')

    if choix == 'manuel':
        somme_totale = {'Gallions' : 0 , 'Mornilles' : 0 , 'Noises' : 0}
    
        gallions = int(input("Combien de gallions faut-il vous rendre ? "))
        mornilles = int(input("De mornilles ? "))
        noises = int(input("De noises ? "))
        
        mornilles += noises // 29 
        gallions += mornilles // 17 
        noises = noises % 29
        mornilles = mornilles % 17
        
        somme_totale['Gallions'], somme_totale['Mornilles'] = gallions, mornilles 
        somme_totale['Noises'] = noises
        
        print('')
        print(f'Ollivander doit vous rendre {somme_totale["Gallions"]} gallions, \
{somme_totale["Mornilles"]} mornilles et {somme_totale["Noises"]} noises.')
        

    if choix == 'test':
        tests = [{'gallions' : 0, 'mornilles' : 0, 'noises' : 0}, {'gallions' : 0, 'mornilles' : 0, 'noises' : 654}, {'gallions' : 0, 'mornilles' : 23, 'noises' : 78}, {'gallions' : 2, 'mornilles' : 11, 'noises' : 9}, {'gallions' : 7, 'mornilles' : 531, 'noises' : 451}]
        tests_originaux = [{'gallions' : 0, 'mornilles' : 0, 'noises' : 0}, {'gallions' : 0, 'mornilles' : 0, 'noises' : 654}, {'gallions' : 0, 'mornilles' : 23, 'noises' : 78}, {'gallions' : 2, 'mornilles' : 11, 'noises' : 9}, {'gallions' : 7, 'mornilles' : 531, 'noises' : 451}]
    
        for i in range(len(tests)):
            tests[i]['mornilles'] += tests[i]['noises'] // 29 
            tests[i]['gallions'] += tests[i]['mornilles'] // 17 
            tests[i]['noises'] = tests[i]['noises'] % 29
            tests[i]['mornilles'] = tests[i]['mornilles'] % 17
    
    print('Voici ce que Ollivander doit vous rendre pour les sommes automatiques :')
    for j in range(len(tests)):
        print('')
        print(f'Quand il doit vous rendre {tests_originaux[j]["gallions"]} \
gallions, {tests_originaux[j]["mornilles"]} mornilles et \
{tests_originaux[j]["noises"]} noises, Ollivander vous rend \
{tests[j]["gallions"]} gallions, {tests[j]["mornilles"]} mornilles \
et {tests[j]["noises"]} noises.')
