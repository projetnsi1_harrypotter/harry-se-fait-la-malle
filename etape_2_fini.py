# -*- coding: utf-8 -*-

somme = int(input("Quelle est la somme a rendre ? "))

def rendre_monnaie(somme):
    """
    cette fonction permet d'exécuter un algorithme qui rend la monnaie de 
    façon optimale.
    
    """
    
    rendu = []
    nb_200 = 1
    nb_100 = 3
    nb_50 = 1
    nb_20 = 1
    nb_10 = 1
    nb_2 = 5

    if somme < 590:
        for i in range(50):
            if somme >= 200 and nb_200 != 0:
                somme = somme - 200
                rendu.append(200)
                nb_200 = nb_200 - 1
            elif somme >= 100 and nb_100 != 0:
                somme = somme - 100
                rendu.append(100)
                nb_100 = nb_100 - 1
            elif somme >= 50 and nb_50 != 0:
                somme = somme - 50
                rendu.append(50)
                nb_50 = nb_50 - 1
            elif somme >= 20 and nb_20 != 0:
                somme = somme - 20
                rendu.append(20)
                nb_20 = nb_20 - 1
            elif somme >= 10 and nb_10 != 0:
                somme = somme - 10
                rendu.append(10)
                nb_10 = nb_10 - 1
            elif somme >= 2 and nb_2 != 0:
                somme = somme - 2
                rendu.append(2)
                nb_2 = nb_2 - 1

        if somme == 1 and nb_2 != 0:
            print("Je n'avais pas la monnaie, je t'ai donc rendu un euro en plus.")
            rendu.append(2)
            nb_2 = nb_2 - 1

        if somme > 1 and nb_100 != 0:
            print("Je n'avais pas la monnaie, je t'ai donc rendu cent euros en plus.")
            rendu.append(100)
            nb_100 = nb_100 - 1

        print(f"Les billets/pièces utilisés sont les suivants : {rendu}")

    else:
        print("Je suis désolé je ne peux pas rendre une telle somme...")
    rendu.clear()

def guipure():
    choix = int(input("Si vous voulez vérifier le programme avec les sommes demandées dans le cahier des charge (0, 8, 62, 231, 497 ,842), tapez 1, si vous préférez entrer la somme à rendre vous mème, tapez 2 : "))

    if choix == 1:

        for somme in (0, 8, 62, 231, 497, 842):
            print(f"\npour {somme} :")
            rendre_monnaie(somme)

    else:

        somme = int(input("\nChoisissez une somme que Mme Guipure devra rendre à Harry : "))
        print("\n")
        rendre_monnaie(somme)
