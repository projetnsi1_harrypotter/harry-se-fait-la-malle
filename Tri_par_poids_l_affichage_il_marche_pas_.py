fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

def tri_par_poids(fournitures):
    for objet in range(len(fournitures) - 1): #j'utilise le tri par selection
        # pour trier le tableau
        poids_minimal = objet
        for objet_min in range(objet + 1 , len(fournitures)):
            if fournitures[objet_min]['Poids'] >= \
                fournitures[poids_minimal]['Poids']:
                poids_minimal = objet_min
        fournitures[objet],fournitures[poids_minimal] = \
            fournitures[poids_minimal], fournitures[objet]
    return fournitures

def malle_par_poids(fournitures):
    malle_tri = tri_par_poids(fournitures)
    malle = []
    poids_maximal = 4
    poids = 0
    for i in range(len(malle_tri)) :
        if malle_tri[i]['Poids'] + poids <= poids_maximal:
            malle.append({'Nom': malle_tri[i]['Nom'], 'Poids':\
                          malle_tri[i]['Poids']})
            poids += malle_tri[i]['Poids']
    return malle

malle_rempli = malle_par_poids(fournitures_scolaires)
poids_total = 0
for i in range(len(malle_rempli)):
    poids_total += malle_rempli[i]['Poids']

print("La malle de Harry contient :\n")
for i in range(len(malle_rempli)):
    print(f"- {malle_rempli[i]['Nom'] :<20} \
({malle_rempli[i]['Poids']} kg)")
print(f'\nLa malle pèse en tout {poids_total}')
