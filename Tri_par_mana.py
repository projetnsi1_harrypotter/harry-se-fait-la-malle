fournitures_scolaires = \
[{'Nom': 'Manuel scolaire', 'Poids': 0.55, 'Mana': 11},
{'Nom': 'Baguette magique', 'Poids': 0.085, 'Mana': 120},
{'Nom': 'Chaudron', 'Poids': 2.5, 'Mana' : 2},
{'Nom': 'Boîte de fioles', 'Poids': 1.2, 'Mana': 4},
{'Nom': 'Téléscope', 'Poids': 1.9, 'Mana': 6},
{'Nom': 'Balance de cuivre', 'Poids': 1.3, 'Mana': 3},
{'Nom': 'Robe de travail', 'Poids': 0.5, 'Mana': 8},
{'Nom': 'Chapeau pointu', 'Poids': 0.7, 'Mana': 9},
{'Nom': 'Gants', 'Poids': 0.6, 'Mana': 25},
{'Nom': 'Cape', 'Poids': 1.1, 'Mana': 13}]
def tri_par_mana(fournitures):
    '''
    Cette fonction permet de trier la mana d'une liste de fourniture par ordre 
    décroissant.
    '''
    for objet in range(len(fournitures) - 1): #j'utilise le tri par selection pour trier le tableau
        mana_minimal = objet
        for objet_min in range(objet + 1 , len(fournitures)):
            if fournitures[objet_min]['Mana'] >= fournitures[mana_minimal]['Mana']:
                mana_minimal = objet_min
        fournitures[objet],fournitures[mana_minimal] = fournitures[mana_minimal], fournitures[objet]
    return fournitures_scolaires


def malle_par_mana(fournitures):
    ''' egerggerg'''
    malle_tri = tri_par_mana(fournitures)
    malle= []
    poids_maximal = 4
    mana = 0
    poids = 0
    for objet in range(len(malle_tri)) :
        if malle_tri[objet]['Poids'] + poids <= poids_maximal:
            malle.append(malle_tri[objet])
            mana += malle_tri[objet]['Mana']
            poids += malle_tri[objet]['Poids']
    print(f"La malle a pour poids total  {poids} kg et une mana de {mana} "
          " elle contient : ")
    return malle
malle_rempli = malle_par_mana(fournitures_scolaires)
print(malle_rempli)


