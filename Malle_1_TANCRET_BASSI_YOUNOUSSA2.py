# -*- coding: utf-8 -*-
"""
Ce prgramme est la réalisation du deuxième mini projet en NSI, Harry se fait 
la malle première partie. 
Il est divisé en plusieures parties, celle avec toutes les fonctions 
permettant d'effectuer les algorithmes. 
Puis on retrouve le coeur du programme. 

Auteurs : Agathe Tancret, Andrea Bassi et Adam Younoussa
Version 1 
Created on Sun Dec 12 17:46:25 2021

"""
def rendre_monnaie(somme):
    """
    cette fonction permet d'exécuter un algorithme qui rend la monnaie de
    façon optimale.
    """

    rendu = []
    nb_200 = 1
    nb_100 = 3
    nb_50 = 1
    nb_20 = 1
    nb_10 = 1
    nb_2 = 5

    if somme < 590:
        for i in range(50):
            if somme >= 200 and nb_200 != 0:
                somme = somme - 200
                rendu.append(200)
                nb_200 = nb_200 - 1
            elif somme >= 100 and nb_100 != 0:
                somme = somme - 100
                rendu.append(100)
                nb_100 = nb_100 - 1
            elif somme >= 50 and nb_50 != 0:
                somme = somme - 50
                rendu.append(50)
                nb_50 = nb_50 - 1
            elif somme >= 20 and nb_20 != 0:
                somme = somme - 20
                rendu.append(20)
                nb_20 = nb_20 - 1
            elif somme >= 10 and nb_10 != 0:
                somme = somme - 10
                rendu.append(10)
                nb_10 = nb_10 - 1
            elif somme >= 2 and nb_2 != 0:
                somme = somme - 2
                rendu.append(2)
                nb_2 = nb_2 - 1

        if somme == 1 and nb_2 != 0:
            print("Je n'avais pas la monnaie, je vous ai donc rendu un euro \
en plus.")
            rendu.append(2)
            nb_2 = nb_2 - 1

        if somme > 1 and nb_100 != 0:
            print("Je n'avais pas la monnaie, je vous ai donc rendu cent \
euros en plus.")
            rendu.append(100)
            nb_100 = nb_100 - 1

        print(f"Les billets et pièces utilisés sont les suivants : {rendu}")

    else:
        print("Je suis désolé je ne peux pas rendre une telle somme...")
    rendu.clear()

# def de adam qu'il n'a pas encore fait et qu'il faut qu'il finisse. 

def guipure():
    choix = int(input("Si vous voulez vérifier le programme avec les sommes \
demandées dans le cahier des charge (0, 8, 62, 231, 497 ,842), tapez 1, si \
vous préférez entrer la somme à rendre vous mème, tapez 2 : "))

    if choix == 1:

        for somme in (0, 8, 62, 231, 497, 842):
            print(f"\npour {somme} :")
            rendre_monnaie(somme)

    else:

        somme = int(input("\nChoisissez une somme que Mme Guipure devra rendre\
 à Harry : "))
        print("\n")
        rendre_monnaie(somme)

def ollivander():
    """
    Cette fonction permet d'exécuter tout ce qu'il se passe
    dans la boutique de ollivander.
    Elle demande d'abord demander à l'utilisateur si il veut rentrer
    sa propre somme à rendre ou si il veut juste faire les tests
    intégrés.
    """

    choix = str(input('Si vous souhaitez essayer ce programme avec les tests \
entrez "test", si vous voulez saisir vous même le montant \
à rendre, saisissez "manuel" : '))
    print('')
    print("Bonjour ! Vous êtes chez Ollivander, un des meilleurs fabricants \
de baguette dans le monde des sorciers. Je me souviens de toutes les baguettes\
 que j'ai vendu dans ma vie. Laissez moi faire et votre baguette vous choisira\
 elle-même.")

    if choix == 'manuel':
        somme_totale = {'Gallions': 0, 'Mornilles': 0, 'Noises': 0}

        gallions = int(input("Combien de gallions faut-il vous rendre ? "))
        mornilles = int(input("De mornilles ? "))
        noises = int(input("De noises ? "))

        mornilles += noises // 29
        gallions += mornilles // 17
        noises = noises % 29
        mornilles = mornilles % 17

        somme_totale['Gallions'] = gallions
        somme_totale['Mornilles'] = mornilles
        somme_totale['Noises'] = noises

        print('')
        print(f'Je dois vous rendre {somme_totale["Gallions"]} gallions, \
{somme_totale["Mornilles"]} mornilles et {somme_totale["Noises"]} noises.')

    if choix == 'test':
        tests = [{'gallions': 0, 'mornilles': 0, 'noises': 0},
                 {'gallions': 0, 'mornilles': 0, 'noises': 654},
                 {'gallions': 0, 'mornilles': 23, 'noises': 78},
                 {'gallions': 2, 'mornilles': 11, 'noises': 9},
                 {'gallions': 7, 'mornilles': 531, 'noises': 451}]
        tests_originaux = [{'gallions': 0, 'mornilles': 0, 'noises': 0},
                           {'gallions': 0, 'mornilles': 0, 'noises': 654},
                           {'gallions': 0, 'mornilles': 23, 'noises': 78},
                           {'gallions': 2, 'mornilles': 11, 'noises': 9},
                           {'gallions': 7, 'mornilles': 531, 'noises': 451}]

        for i in range(len(tests)):
            tests[i]['mornilles'] += tests[i]['noises'] // 29
            tests[i]['gallions'] += tests[i]['mornilles'] // 17
            tests[i]['noises'] = tests[i]['noises'] % 29
            tests[i]['mornilles'] = tests[i]['mornilles'] % 17

    print('Voici ce que Ollivander doit vous rendre pour les sommes \
automatiques :')
    for j in range(len(tests)):
        print('')
        print(f'Quand il doit vous rendre {tests_originaux[j]["gallions"]} \
gallions, {tests_originaux[j]["mornilles"]} mornilles et \
{tests_originaux[j]["noises"]} noises, Ollivander vous rend \
{tests[j]["gallions"]} gallions, {tests[j]["mornilles"]} mornilles \
et {tests[j]["noises"]} noises.')

def chemin_de_traverse():
    print("Vous êtes à présent dans le chemin de traverse pour \
    faire des achats, choisissez dans quelle boutique vous souhaitez aller.")
    boutique = int(input("Pour aller chez Fleury and Bott, tapez 1, si vous \
souhaitez plutot acheter vos robes chez Madame Guipure, taper 2, ou \
si préférez choisir votre baguette, taper 3 : "))

    if boutique == 1:
        # def de adam
        print('ADAAAAAAM FAIT TON PROGRAMME MERDE !')

    if boutique == 2:
        guipure()

    if boutique == 3: 
        ollivander()

    else:
        print("Veuillez choisir un magasin proposé. Nous savons que \
vous mourez d'envie d'aller chez les frères Weasleys mais vous ne pouvez pas. \
Il faut que vous fassiez vos courses scolaires.")


chemin_de_traverse()