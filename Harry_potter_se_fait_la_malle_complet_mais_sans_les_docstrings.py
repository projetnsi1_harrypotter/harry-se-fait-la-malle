# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 19:53:07 2022

@author: tancret agathe 
"""
fournitures_scolaires = \
[{'Nom': 'Manuel scolaire', 'Poids': 0.55, 'Mana': 11},
{'Nom': 'Baguette magique', 'Poids': 0.085, 'Mana': 120},
{'Nom': 'Chaudron', 'Poids': 2.5, 'Mana': 2},
{'Nom': 'Boîte de fioles', 'Poids' : 1.2, 'Mana': 4},
{'Nom': 'Téléscope', 'Poids': 1.9, 'Mana': 6},
{'Nom': 'Balance de cuivre', 'Poids': 1.3, 'Mana': 3},
{'Nom': 'Robe de travail', 'Poids': 0.5, 'Mana': 8},
{'Nom': 'Chapeau pointu', 'Poids': 0.7, 'Mana': 9},
{'Nom': 'Gants', 'Poids': 0.6, 'Mana': 25},
{'Nom': 'Cape', 'Poids': 1.1, 'Mana': 13}]

def tri_par_poids(fournitures):
    for objet in range(len(fournitures) - 1):  # j'utilise le tri par selection
        # pour trier le tableau
        poids_minimal = objet
        for objet_min in range(objet + 1, len(fournitures)):
            if fournitures[objet_min]['Poids'] >= \
                    fournitures[poids_minimal]['Poids']:
                poids_minimal = objet_min
        fournitures[objet], fournitures[poids_minimal] = \
            fournitures[poids_minimal], fournitures[objet]
    return fournitures

def tri_par_mana(fournitures):
    for objet in range(len(fournitures) - 1):  # j'utilise le tri par
        # selection pour trier le tableau
        mana_minimal = objet
        for objet_min in range(objet + 1, len(fournitures)):
            if fournitures[objet_min]['Mana'] >= \
                    fournitures[mana_minimal]['Mana']:
                mana_minimal = objet_min
        fournitures[objet], fournitures[mana_minimal] = \
            fournitures[mana_minimal], fournitures[objet]
    return fournitures

def remplir_malle(poids_max, fournitures):
    contenu_malle = []
    poids_malle = 0
    for i in range(len(fournitures)):
        if fournitures[i]['Poids'] + poids_malle < poids_max:
            contenu_malle.append(fournitures[i])
            poids_malle += fournitures[i]['Poids']
    return(contenu_malle, poids_malle)

def malle_par_poids(fournitures):
    malle_triee = tri_par_poids(fournitures)
    malle = []
    poids_maximal = 4
    poids = 0
    for i in range(len(malle_triee)):
        if malle_triee[i]['Poids'] + poids <= poids_maximal:
            malle.append({'Nom': malle_triee[i]['Nom'], 'Poids':
                          malle_triee[i]['Poids']})
            poids += malle_triee[i]['Poids']
    return (malle, poids)

def malle_par_mana(fournitures):
    malle_tri = tri_par_mana(fournitures)
    malle = []
    poids_maximal = 4
    mana = 0
    poids = 0
    for i in range(len(malle_tri)):
        if malle_tri[i]['Poids'] + poids <= poids_maximal:
            malle.append(malle_tri[i])
            mana += malle_tri[i]['Mana']
            poids += malle_tri[i]['Poids']
    return (poids, mana, malle)

malle_remplie_au_pif = remplir_malle(4, fournitures_scolaires)
malle_remplie_par_poids = malle_par_poids(fournitures_scolaires)
malle_remplie_par_mana = malle_par_mana(fournitures_scolaires)

print("Grace à ce programme, Harry va pouvoir remplir sa malle de trois façon \
différentes :\n")
print("- La première façon de remplir la malle est de façon la plus simple \
c'est-à-dire en mettant des objets au hasard dans la malle sans jamais dépacer\
 le poids maximal.\n")
print("- La deuxième façon de procéder est de remplir la malle de façon la \
optimale, c'est-à-dire remplir une malle la plus lourde possible.\n")
print("- La troisième manière pour Harry de remplir sa malle est d'y mettre \
le maximun de mana (énergie magique) possible sans pour autant dépasseer \
le poids maximal. \n")

print("======================================================================")

print("Quand on Harry remplit sa malle de la première façcon,\
la malle de Harry contient alors :\n")
for i in range(len(malle_remplie_au_pif) + 2):
    print(f"- {malle_remplie_au_pif[0][i]['Nom'] :<20} \
({malle_remplie_au_pif[0][i]['Poids']} kg)")
print(f"\nLa malle pèse en tout {malle_remplie_au_pif[1]} kg.\n")

print("======================================================================")

print("En utilisant la deuxième manière de remplir la malle, la malle de \
Harry contient :\n")
for i in range(len(malle_remplie_par_poids[0])):
    print(f"- {malle_remplie_par_poids[0][i]['Nom'] :<20} \
(Poids : {malle_remplie_par_poids[0][i]['Poids']} kg)")
print(f'\nLa malle pèse en tout {malle_remplie_par_poids[1]}\n')

print("======================================================================")

print("Avec la troisième technique de remplissage, la malle d'Harry contient \
les fournitures suivante : \n")
for materiel in malle_remplie_par_mana[2]:
    print(f"- {materiel['Nom'] :<20} (Poids : {materiel['Poids'] :<5} \
Mana: {materiel['Mana']} ) ")
print(f'\nLa malle possède un mana de {malle_remplie_par_mana[1]} et pèse en \
tout {malle_remplie_par_mana[0]} kg.')




